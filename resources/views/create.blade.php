@extends('layouts/base')

@section('content')
<form class="client-form" method="post" action="/create">
    <h3 class="client-form__head">Add Client</h3>
    @include('partials/client-form')
</form>
@endsection
