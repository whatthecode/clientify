<!DOCTYPE html>
<html>
    <head>
        <title>Clientify</title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/css/app.css">
    </head>
    <body>

		<header class="header">

			<div class="container">
				<div class="header__brand">
					<a href="/"><span>Clientify</span> <i class="fa fa-users"></i></a>
				</div>
			</div>

		</header>

		<main class="main-content">

			@yield('content')

		</main>
	<script src="/js/main.js"></script>
    </body>
</html>
