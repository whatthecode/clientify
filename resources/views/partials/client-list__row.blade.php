<tr>
	@if($client->sticky)
	<td class="client-list__row__cell--active">
		<a href="/sticky-toggle/{{ $client->id }}"><i class="fa fa-thumb-tack"></i></a>
	</td>
	@else
	<td class="client-list__row__cell">
		<a href="/sticky-toggle/{{ $client->id }}"><i class="fa fa-thumb-tack"></i></a>
	</td>
	@endif
	<td>
		{{ $client->name }}
	</td>
	<td>
		<a href="tel:{{ $client->phone }}"><i class="fa fa-phone"></i></a>
		<a href="mailto:{{ $client->contact_email }}"><i class="fa fa-envelope"></i></a>
		<a href="/edit/{{ $client->id }}"><i class="fa fa-pencil-square-o"></i></a>
	</td>
	<td>{{ $client->company }}</td>
	<td>{{ $client->website }}</td>
	<td>{{ $client->services->lists('name')->implode(', ') }}</td>
</tr>
