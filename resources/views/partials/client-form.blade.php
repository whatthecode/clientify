@inject('services', 'App\Service')
{{ csrf_field() }}

<div class="row">
	<div class="col-md-6">

		<div class="form-group">
		    <label for="status" class="col-sm-3 control-label">Status</label>
		    <div class="col-sm-9">
		        <select name="status" class="form-control">
		        	<option>Active</option>
		        	<option>Quote in Progress</option>
		        	<option>Quote Submitted</option>
		        </select>
		    </div>
		</div>

	</div>
</div>
<div class="row">

<div class="col-md-6">
    <div class="form-group">
        <label for="client_name" class="col-sm-3 control-label">Name</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="name" id="client_name" placeholder="Full Name" value="{{ $client->name }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_company" class="col-sm-3 control-label">Company</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_company" name="company" placeholder="Company" value="{{ $client->company }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_website" class="col-sm-3 control-label">Website</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_website" name="website" placeholder="Website" value="{{ $client->website }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_email" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
            <input type="email" class="form-control" id="client_email" name="contact_email" placeholder="Email" value="{{ $client->contact_email }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_phone" class="col-sm-3 control-label">Phone</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_phone" name="phone" placeholder="Phone" value="{{ $client->phone }}">
        </div>
    </div>
</div>
<!-- ./col-md-6 -->
<div class="col-md-6">
    <div class="form-group">
        <label for="client_street" class="col-sm-3 control-label">Street</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_street" name="street" placeholder="Street" value="{{ $client->street }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_city" class="col-sm-3 control-label">City</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_city" name="city" placeholder="City" value="{{ $client->city }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_state" class="col-sm-3 control-label">State</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_state" name="state" placeholder="State" value="{{ $client->state }}">
        </div>
    </div>
    <div class="form-group">
        <label for="client_zipcode" class="col-sm-3 control-label">Zipcode</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="client_zipcode" name="zipcode" placeholder="Zipcode" value="{{ $client->zipcode }}">
        </div>
    </div>
</div>
<!-- ./col-md-6 -->
</div>
<div class="row">
	<div class="col-md-6">
	    <div class="form-group">
	    	<label for="client_services" class="col-sm-3 control-label">Services</label>
	        <div class="col-sm-9">
	            @foreach($services->all() as $service)
	            	<div class="checkbox">
	            	  <label>
	            	    <input type="checkbox" name="services[]" value="{{ $service->name }}"
							@if(in_array($service->name, $selectedServices))
								checked
							@endif
	            	    >
	            	    {{ $service->name }}
	            	  </label>
	            	</div>
	            @endforeach
	        </div>
	    </div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	    <div class="form-group">
	        <div class="col-sm-offset-3 col-sm-9">
	            <button type="submit" class="btn btn-primary btn-lg">Save</button>
	        </div>
	    </div>
	</div>
</div>
