@extends('layouts/base')
@section('content')
<form class="client-form" method="post" action="/edit/{{$client->id}}">
    <h3 class="client-form__head">Edit Client</h3>

    @include('partials/client-form', $client)

    <div class="row">
    	<div class="col-md-6">
    	    <div class="form-group">
    	        <div class="col-sm-offset-3 col-sm-9">
    	            <div class="alert alert-danger">
    	                <label>
    	                    <input type="checkbox" name="fired" value="1"> FIRE THIS CLIENT!
    	                </label>
    	            </div>
    	        </div>
    	    </div>
    	</div>
    </div>

</form>
@endsection
