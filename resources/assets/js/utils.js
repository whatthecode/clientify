import $ from 'jquery'

const Utils = {
	stickySorter: function(a, b) {
  		if (!b.sticky && a.sticky) {
  			return -1
  		}
  		if (b.sticky && !a.sticky) {
  			return 1
  		}
  		return 0
  	},

  	objectify: function($form) {

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        var build = function(base, key, value){
            base[key] = value;
            return base;
        };

        var push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($form.serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = build([], push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    }
}

module.exports = Utils
