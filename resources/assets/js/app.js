import React from 'react'
import ClientList from './components/ClientList'
import ClientForm from './components/ClientForm'
import $ from 'jquery'
import _ from 'lodash'
import classNames from 'classnames'

const defaultServices = {
  'Hosting': false,
  'Design': false,
  'Maintanence': false,
  'Backups': false,
  'SEO': false
}

const newClientTemplate = {
  id: '',
  name: '',
  company: '',
  website: '',
  contact_email: '',
  phone: '',
  street: '',
  city: '',
  state: '',
  zipcode: '',
  status: 'Quote in Progress',
  sticky: false,
  services: _.clone(defaultServices)
}

var App = React.createClass({
  propTypes: {
    url: React.PropTypes.string
  },

  _populateClientForm: function (clientIndex, e) {
    e.preventDefault()
    this.setState({
      selectedClient: clientIndex,
      clientFormOpened: true
    })
  },

  _handleEscKey: function (event) {
    if (event.keyCode === 27) {
      this._closeFormPanel()
    }
  },
  componentWillMount: function () {
    $(document).on('keydown', this._handleEscKey)
  },

  componentWillUnmount: function () {
    $(document).off('keydown', this._handleEscKey)
  },

  componentDidMount: function () {
    this._fetchClientList()
  },

  _fetchClientList: function () {
    let request = $.ajax({
      url: this.props.url,
      dataType: 'json',
      method: 'GET'
    })

    this.setState({
      dataIsSyncing: true
    })

    request.then((clients) => this.setState({
      dataIsSyncing: false,
      clients
    }))
  },

  getInitialState: function () {
    return {
      dataIsSyncing: false,
      clients: [],
      selectedClient: false,
      clientFormOpened: false
    }
  },

  _onFormChange: function (clientIndex, formData) {
    let clients = this.state.clients
    clients[clientIndex] = formData
    this.setState({clients})
  },

  _handleFormSubmit: function (clientData) {
    this.setState({
      selectedClient: false,
      clientFormOpened: false
    })

    this._syncClient(clientData)
  },

  _syncClient: function (clientData) {
    let request = $.ajax({
      url: this.props.url,
      data: clientData,
      dataType: 'json',
      method: 'POST'
    })

    this.setState({dataIsSyncing: true})

    request.then((clients) => this.setState({
      dataIsSyncing: false,
      clients
    }))
  },

  _onDeleteClient: function (clientIndex, e) {
    e.preventDefault()
    let clients = this.state.clients
    let id = clients[clientIndex].id
    if (window.confirm('Are you sure you want to fire this client?')) {
      clients.splice(clientIndex, 1)
      this.setState({clients})
      this._deleteClient(id)
    }
  },

  _deleteClient: function (id) {
    let request = $.ajax({
      url: this.props.url + '/' + id,
      method: 'DELETE'
    })

    this.setState({
      dataIsSyncing: true
    })

    request.then((clients) => this.setState({
      dataIsSyncing: false,
      clients
    }))
  },

  _changeClientStatus: function (newStatus, clientIndex) {
    // set status of client
    //
    var client = this.state.clients[clientIndex]
    client.status = newStatus
    this.setState({
      clients: this.state.clients
    })

    this._syncClient(client)
  },

  _addNewClient: function () {
    let clients = this.state.clients.concat(newClientTemplate)

    this.setState({
      selectedClient: clients.length - 1,
      clientFormOpened: true,
      clients: clients
    })
  },

  _closeFormPanel: function () {
    let clients = this.state.clients.filter((client) => {
      return (client.id)
    })
    this.setState({clientFormOpened: false, clients})
  },

  _toggleSticky: function (clientIndex) {
    var client = this.state.clients[clientIndex]
    client.sticky = !client.sticky
    this.setState({
      clients: this.state.clients
    })
    this._syncClient(client)
  },

  _seedData: function () {
    let request = $.ajax({
      url: this.props.url + '/seed',
      method: 'GET'
    })

    this.setState({dataIsSyncing: true})

    request.then((clients) => this.setState({
      dataIsSyncing: false,
      clients
    }))
  },

  render: function () {
    let clientFormData = newClientTemplate
    let clientIndex = null
    let className = classNames({
      'client-form-panel': true,
      'active': this.state.clientFormOpened
    })
    if (this.state.selectedClient !== false) {
      clientFormData = this.state.clients[this.state.selectedClient]
      clientIndex = this.state.selectedClient
    }
    return (
    <div className='client-app' id='app'>
      <div className='col-md-3'>
        <div className='add-client-button'>
          <a href='#' onClick={this._addNewClient} className='btn btn-primary'><i className='fa fa-plus'></i> Add</a>
        </div>
      </div>
      <div className='col-md-3'>
        <a href='#' onClick={this._seedData}>Seed Clients</a>
      </div>
      {this.state.dataIsSyncing ? <div className='sync-status'>Syncing...</div> : null}
      <ClientList
        clients={this.state.clients}
        toggleSticky={this. _toggleSticky}
        changeClientStatus={this._changeClientStatus}
        populateClientForm={this._populateClientForm}
        deleteClient={this._onDeleteClient} />
      <div className={className}>
        {this.state.clientFormOpened ? <ClientForm
                                         onFormChange={this._onFormChange}
                                         closeFormPanel={this._closeFormPanel}
                                         handleFormSubmit={this._handleFormSubmit}
                                         clientIndex={clientIndex}
                                         formData={clientFormData} /> : null}
      </div>
    </div>
    )
  }
})

export default App
