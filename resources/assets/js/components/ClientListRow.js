import React from 'react'
import cx from 'classnames'

import StatusDropdown from './StatusDropdown'

const ClientListRow = React.createClass({

  propTypes: {
    client: React.PropTypes.object,
    clientIndex: React.PropTypes.number,
    handleStatusChange: React.PropTypes.func,
    populateClientForm: React.PropTypes.func,
    deleteClient: React.PropTypes.func,
    sticky: React.PropTypes.bool,
    toggleSticky: React.PropTypes.func
  },

  _toggleSticky: function (e) {
    e.preventDefault()
    this.props.toggleSticky(this.props.clientIndex)
  },

  _renderServices: function (serviceKey) {
    if (this.props.client.services[serviceKey]) {
      return true
    }
    return false
  },

  render: function () {
    let client = this.props.client
    let stickyClass = cx({
      'active': client.sticky
    })

    return (
    <tr>
      <td className='client-list__row__cell'>
        <a href='#' onClick={this._toggleSticky} className={stickyClass}><i className='fa fa-thumb-tack'></i></a>
      </td>
      <td className='client-list__row__cell'>
        {client.name}
      </td>
      <td className='client-list__row__cell' width='150'>
        <a href={'tel:' + client.phone}><i className='fa fa-phone'></i></a>
        <a href={'mailto:' + client.contact_email}><i className='fa fa-envelope'></i></a>
        <a href='#' onClick={this.props.populateClientForm.bind(null, this.props.clientIndex)}><i className='fa fa-pencil-square-o'></i></a>
        <a href='#' onClick={this.props.deleteClient.bind(null, this.props.clientIndex)}><i className='fa fa-remove' style={{color: '#c00'}}></i></a>
      </td>
      <td>
        <StatusDropdown
          title={client.status}
          items={['Quote in Progress', 'Quote Completed', 'Active']}
          isOpen={client.statusIsOpen}
          handleSelectStatus={this.props.handleStatusChange}
          clientIndex={this.props.clientIndex} />
      </td>
      <td className='client-list__row__cell'>
        {client.company}
      </td>
      <td className='client-list__row__cell'>
        {client.website}
      </td>
      <td className='client-list__row__cell'>
        {Object.keys(client.services).filter(this._renderServices).join(', ')}
      </td>
    </tr>
    )
  }
})

export default ClientListRow
