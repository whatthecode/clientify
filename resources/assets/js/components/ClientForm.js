import React from 'react'

var ClientForm = React.createClass({
  propTypes: {
    handleFormSubmit: React.PropTypes.func,
    clientIndex: React.PropTypes.node,
    formData: React.PropTypes.object,
    onFormChange: React.PropTypes.func,
    closeFormPanel: React.PropTypes.func
  },

  _renderServices: function (service, i) {
    let checked = this.props.formData.services[service]
    return (
    <div className='checkbox' key={i}>
      <label>
        <input
          type='checkbox'
          name='services'
          ref={'services[' + service + ']'}
          checked={checked}
          onChange={this._handleChange}
          value={service} />
        {service}
      </label>
    </div>
    )
  },

  _handleChange: function () {
    this.props.onFormChange(this.props.clientIndex, this._getFormData())
  },

  _getSelectedServices: function () {
    let selectedServices = {}
    Object.keys(this.props.formData.services).forEach((key) => {
      selectedServices[key] = !!this.refs['services[' + key + ']'].checked
    })
    return selectedServices
  },

  _getFormData: function () {
    let data = {
      id: this.refs.id ? this.refs.id.value : null,
      name: this.refs.name.value,
      company: this.refs.company.value,
      website: this.refs.website.value,
      contact_email: this.refs.contact_email.value,
      phone: this.refs.phone.value,
      street: this.refs.street.value,
      city: this.refs.city.value,
      state: this.refs.state.value,
      zipcode: this.refs.zipcode.value,
      status: this.refs.status.value,
      services: this._getSelectedServices()
    }
    return data
  },

  _handleFormSubmit: function (e) {
    e.preventDefault()
    this.props.handleFormSubmit(this._getFormData())
  },

  render: function () {
    let formData = this.props.formData
    return (

    <form className='client-form' method='post' onSubmit={this._handleFormSubmit}>
      {formData.id ? <input
                       type='hidden'
                       name='id'
                       ref='id'
                       value={formData.id} /> : null}
      <input
        type='hidden'
        name='status'
        ref='status'
        value={formData.status} />
      <h3 className='client-form__head'>Edit Client</h3>
      <div className='row'>
        <div className='col-md-6'>
          <div className='form-group'>
            <label htmlFor='client_name' className='col-sm-3 control-label'>
              Name
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='name'
                ref='name'
                placeholder='Full Name'
                onChange={this._handleChange}
                value={formData.name} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_company' className='col-sm-3 control-label'>
              Company
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='company'
                ref='company'
                placeholder='Company'
                onChange={this._handleChange}
                value={formData.company} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_website' className='col-sm-3 control-label'>
              Website
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='website'
                ref='website'
                placeholder='Website'
                onChange={this._handleChange}
                value={formData.website} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_email' className='col-sm-3 control-label'>
              Email
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='contact_email'
                ref='contact_email'
                placeholder='Email'
                onChange={this._handleChange}
                value={formData.contact_email} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_phone' className='col-sm-3 control-label'>
              Phone
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='phone'
                ref='phone'
                placeholder='Phone'
                onChange={this._handleChange}
                value={formData.phone} />
            </div>
          </div>
        </div>
        <div className='col-md-6'>
          <div className='form-group'>
            <label htmlFor='client_street' className='col-sm-3 control-label'>
              Street
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='street'
                ref='street'
                placeholder='Street'
                onChange={this._handleChange}
                value={formData.street} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_city' className='col-sm-3 control-label'>
              City
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='city'
                ref='city'
                placeholder='City'
                onChange={this._handleChange}
                value={formData.city} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_state' className='col-sm-3 control-label'>
              State
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='state'
                ref='state'
                placeholder='State'
                onChange={this._handleChange}
                value={formData.state} />
            </div>
          </div>
          <div className='form-group'>
            <label htmlFor='client_zipcode' className='col-sm-3 control-label'>
              Zipcode
            </label>
            <div className='col-sm-9'>
              <input
                type='text'
                className='form-control'
                name='zipcode'
                ref='zipcode'
                placeholder='Zipcode'
                onChange={this._handleChange}
                value={formData.zipcode} />
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-md-6'>
          <div className='form-group'>
            <label htmlFor='client_services' className='col-sm-3 control-label'>
              Services
            </label>
            <div className='col-sm-9'>
              {Object.keys(formData.services).map(this._renderServices)}
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-md-6'>
          <div className='form-group'>
            <div className='col-sm-offset-3 col-sm-9'>
              <button type='submit' className='btn btn-primary btn-lg'>
                Save
              </button>
              <button type='button' onClick={this.props.closeFormPanel} className='btn btn-link btn-lg'>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
      <pre><code>{JSON.stringify(formData, null, '  ')}</code></pre>
    </form>
    )
  }
})

export default ClientForm
