import React from 'react'
import cx from 'classnames'

const StatusDropdown = React.createClass({

  getInitialState: function() {
  	return {
  		isOpen: false
  	};
  },

  toggleDropdown: function(e) {
  	e.preventDefault();
  	this.setState({
  		isOpen: !this.state.isOpen
  	});
  },

  handleSelectStatus: function(status) {
  	return function(e) {
	  	this.setState({
	  		isOpen: false
	  	});
  		this.props.handleSelectStatus(status, this.props.clientIndex);
  	}.bind(this)
  },

  render: function() {
	var items = this.props.items.map(function(item, i) {
		var className = cx({active: this.props.title == item});
		return <li key={i} className={className}><a onClick={this.handleSelectStatus(item)}>{item}</a></li>
	}.bind(this));
	var classes = cx({
		dropdown: true,
		"status-dropdown": true,
		open: this.state.isOpen
	});
	return (
		<div className={classes}>
		  <button onClick={this.toggleDropdown} className="btn btn-default btn-sm" type="button" data-toggle="dropdown">
		    {this.props.title}
		    <span className="caret"></span>
		  </button>
		  <ul className="dropdown-menu">
		    {items}
		  </ul>
		</div>
	)
  }
});

export default StatusDropdown
