var React = require('react')
import ClientListRow from './ClientListRow'

var ClientList = React.createClass({

  propTypes: {
    changeClientStatus: React.PropTypes.func,
    populateClientForm: React.PropTypes.func,
    deleteClient: React.PropTypes.func,
    toggleSticky: React.PropTypes.func,
    clients: React.PropTypes.array
  },

  renderClientRow: function (client, i) {
    return <ClientListRow
             key={i}
             clientIndex={i}
             client={client}
             handleStatusChange={this.props.changeClientStatus}
             toggleSticky={this.props.toggleSticky}
             populateClientForm={this.props.populateClientForm}
             deleteClient={this.props.deleteClient} />
  },
  render: function () {
    return (
    <table className='client-list'>
      <thead className='client-list__head'>
        <tr>
          <th></th>
          <th>
            Name
          </th>
          <th></th>
          <th>
            Status
          </th>
          <th>
            Company
          </th>
          <th>
            Website
          </th>
          <th>
            Services
          </th>
        </tr>
      </thead>
      <tbody className='client-list__body'>
        {this.props.clients.map(this.renderClientRow)}
      </tbody>
    </table>
    )
  }

})

module.exports = ClientList
