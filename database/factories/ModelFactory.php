<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => 'admin',
        'password' => bcrypt('admin'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'contact_email' => $faker->safeEmail,
        'company' => $faker->company,
        'website' => $faker->domainName,
        'phone' => $faker->phoneNumber,
        'street' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'zipcode' => $faker->postcode,
        'status' => $faker->randomElement(['Quote in Progress', 'Quote Completed', 'Active'])
    ];
});
