<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Client', 10)->create()->each(function ($client) {
            $start = rand(1, 5);
            for ($i = $start; $i <= rand($start, 5); ++$i) {
                $client->services()->attach($i);
            }

        });
    }
}
