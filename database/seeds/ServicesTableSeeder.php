<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Service::create(['name' => 'Hosting']);
        App\Service::create(['name' => 'Design']);
        App\Service::create(['name' => 'Maintanence']);
        App\Service::create(['name' => 'Backups']);
        App\Service::create(['name' => 'SEO']);
    }
}
