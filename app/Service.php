<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    public static $services = [
      'Hosting' => false,
      'Design' => false,
      'Maintanence' => false,
      'Backups' => false,
      'SEO' => false,
    ];

    /**
     * Get clients associated with service
     * @return [type] [description]
     */
    public function clients()
    {
        return $this->belongsToMany('App\Client');
    }
}
