<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\Service;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->getClientsWithServices();
    }

    protected function getClientsWithServices()
    {
        sleep(1);
        $clients = \App\Client::with('services')
          ->orderBy('sticky', 'desc')
          ->orderBy('name', 'asc')
          ->get();
        return $clients->map(function($client) {
          $services = \App\Service::$services;
          return [
            'id' => $client->id,
            'name' => $client->name,
            'company' => $client->company,
            'website' => $client->website,
            'contact_email' => $client->contact_email,
            'phone' => $client->phone,
            'street' => $client->street,
            'city' => $client->city,
            'state' => $client->state,
            'zipcode' => $client->zipcode,
            'status' => $client->status,
            'sticky' => !!$client->sticky,
            'services' => $this->mapServices(\App\Service::$services, $client->services),
          ];
        });
    }

    protected function mapServices($services, $selectedServices)
    {
      return collect($services)->map(function($service, $key) use ($selectedServices) {
        return (in_array($key, $selectedServices->pluck('name')->toArray()));
      });
    }

    public function seedClients()
    {

      Client::truncate();

      factory('App\Client', 10)->create()->each(function ($client) {
          $start = rand(1, 5);
          for ($i = $start; $i <= rand($start, 5); ++$i) {
              $client->services()->attach($i);
          }
      });
      return $this->getClientsWithServices();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('id', false);
        if ($id) {
          $client = Client::findOrFail($id);
        } else {
          $client = new Client;
        }

        $client->name = $request->get('name');
        $client->company = $request->get('company');
        $client->website = $request->get('website');
        $client->contact_email = $request->get('contact_email');
        $client->phone = $request->get('phone');
        $client->street = $request->get('street');
        $client->city = $request->get('city');
        $client->state = $request->get('state');
        $client->zipcode = $request->get('zipcode');
        $client->status = $request->get('status', 'Quote in Progress');
        $client->sticky = b($request->get('sticky', false));
        // dd($request->get('services'), $client->services()->get()->toArray());
        $hosting = Service::whereName('Hosting')->first();

        $services = $request->get('services', []);
        $attachServices = collect([]);
        foreach($services as $service => $active) {
          if (b($active)) {
            $attachServices->push(Service::whereName($service)->first());
          }
        }
        $client->services()->sync([]);
        $client->save();
        $client->services()->saveMany($attachServices);

        return $this->getClientsWithServices();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        $client->delete();
        return $this->getClientsWithServices();
    }

    public function updateStatus(Request $request, $client) {
      $client = Client::find($client);
      $client->status = $request->input('status');
      $client->save();
      return ['done' => true];
    }
}
