<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
use Illuminate\Http\Request;

Route::group(['middleware' => ['api'], 'prefix' => 'api'], function () {

    Route::get('client/seed', 'ClientController@seedClients');
    Route::resource('client', 'ClientController');

    Route::put('client/{client}/status', 'ClientController@updateStatus');
});

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome', [
            'clients' => App\Client::with('services')
                ->where('fired', '!=', true)
                ->orderBy('sticky', 'desc')
                ->orderBy('name', 'asc')->get(),
        ]);
    });

    Route::get('/edit/{client}', function (App\Client $client) {
        return view('edit', [
            'client' => $client,
            'selectedServices' => collect($client->services->lists('name'))->toArray(),
        ]);
    });

    Route::post('/edit/{client}', function (App\Client $client, Request $request) {
        $client->name = $request->get('name');
        $client->company = $request->get('company');
        $client->website = $request->get('website');
        $client->contact_email = $request->get('contact_email');
        $client->phone = $request->get('phone');
        $client->street = $request->get('street');
        $client->city = $request->get('city');
        $client->state = $request->get('state');
        $client->zipcode = $request->get('zipcode');
        $client->fired = !!$request->get('fired', false);

        // get services
        $services = collect(App\Service::whereIn('name', $request->get('services'))->lists('id'))->toArray();
        $client->services()->sync($services);
        $client->save();
        return redirect('/');
    });

    Route::post('/create', function (Request $request) {

        $client = new App\Client;

        $client->name = $request->get('name');
        $client->company = $request->get('company');
        $client->website = $request->get('website');
        $client->contact_email = $request->get('contact_email');
        $client->phone = $request->get('phone');
        $client->street = $request->get('street');
        $client->city = $request->get('city');
        $client->state = $request->get('state');
        $client->zipcode = $request->get('zipcode');

        // get services
        $services = collect(App\Service::whereIn('name', $request->get('services'))->lists('id'))->toArray();
        $client->save();
        $client->services()->sync($services);
        return redirect('/');
    });

    Route::get('/create/', function () {
        return view('create', [
            'client' => (object) [
                'name' => '',
                'company' => '',
                'website' => '',
                'contact_email' => '',
                'phone' => '',
                'street' => '',
                'city' => '',
                'state' => '',
                'zipcode' => '',
            ],
            'selectedServices' => [],
        ]);
    });

    Route::get('/sticky-toggle/{client}', function (App\Client $client) {

        $client->sticky = !$client->sticky;
        $client->save();

        return back();
    });

    Route::get('/services', function () {
        App\Service::create(['name' => 'Hosting']);
        App\Service::create(['name' => 'Design']);
        App\Service::create(['name' => 'Maintanence']);
        App\Service::create(['name' => 'Backups']);
        App\Service::create(['name' => 'SEO']);
        return redirect('/');
    });
});
