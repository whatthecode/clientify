<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Get services associated with client
     * @return [type] [description]
     */
    public function services()
    {
        return $this->belongsToMany('App\Service')->withTimestamps();
    }
}
