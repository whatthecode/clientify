# Clientify Simple CRM

For training purposes

### Installing for the first time

Run these commands:

(First make sure you've installed composer)
https://getcomposer.org/

After you've installed composer, run these commands:
```
git clone https://bitbucket.org/whatthecode/clientify.git
cd clientify
cp .env.example .env
php artisan key:generate
chmod -R 777 storage
npm install
gulp
touch database/database.sqlite
composer install
php artisan migrate
php artisan db:seed
php artisan serve
```

Then browse to http://localhost:8000

The master branch is the base application with everything being done as a server side application. The lessons are done via branches, with each building up to a full SPA application using React.

## LESSONS ##

### Lesson 1: ###

Checkout the lesson one branch:

```
git checkout lesson-01
```